from rest_framework.views import APIView
from rest_framework.response import Response
from .models import User
from .serializers import (
    RegisterSerializer,
    UserInfoSerializer,
)
from rest_framework import generics
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework_simplejwt.views import TokenObtainPairView
from user.adapters import (
    raise_error, response_,
)
from rest_framework_simplejwt.serializers import TokenObtainSerializer
from rest_framework_simplejwt.tokens import RefreshToken
from datetime import datetime, timedelta


class TokenObtainPairSerializer(TokenObtainSerializer):

    @classmethod
    def get_token(cls, user):
        return RefreshToken.for_user(user)

    def validate(self, attrs):
        data = super().validate(attrs)

        refresh = self.get_token(self.user)
        self.user.last_login_at = datetime.utcnow()
        self.user.save()

        data['email'] = self.user.email
        data['username'] = self.user.username

        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)

        return data

class LoginAPI(TokenObtainPairView):
    """
        Api should login user and give token
    """
    serializer_class = TokenObtainPairSerializer

class RegisterView(generics.CreateAPIView):
    """
        Api should registration user
    """
    queryset = User.objects.all()
    permission_classes = (AllowAny,)
    serializer_class = RegisterSerializer


class LogoutView(APIView):
    """
        Api should logout user
    """
    def post(self, request):
        response = Response()
        response.data = {
            'message': 'success'
        }
        return response

class UserInfoAPI(APIView):
    """
        API for user info
    """
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        current_user = request.user
        user_data = User.objects.filter(id=current_user.id).first()

        serializer = UserInfoSerializer(user_data)

        return response_(serializer.data, 200)
