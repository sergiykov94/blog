from django.urls import path
from rest_framework_simplejwt.views import TokenRefreshView
from user.views import (
    LoginAPI,
    RegisterView, 
    LogoutView,
    UserInfoAPI,
)

app_name = 'user'

urlpatterns = [
    path('login/', LoginAPI.as_view(), name='token_obtain_pair'),
    path('login/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('register/', RegisterView.as_view(), name='user_register'),
    path('logout/', LogoutView.as_view(), name='user_logout'),
    path('user-info/', UserInfoAPI.as_view(), name='user-info'),
]
