"""
    Adapters
"""


def response_(data: str, status):
    """
        Response function
    """
    from rest_framework.response import Response
    return Response(data, status)

def raise_error(code: int, text: str):
    """
        Function should raise error
    """
    from errors import raise_error
    raise raise_error(code, text)