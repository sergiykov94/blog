"""
    Test post module
"""

import pytest
from freezegun import freeze_time
import datetime

@pytest.mark.django_db
class TestPostApi:

    def test_should_create_post(self, client, employee_user, auth_headers, create_test_image, post_model):
        """
            Test should create post
        """
        user = employee_user
        headers = auth_headers(user)
        payload = {
            'title' : 'some_title',
            'body' : 'some_body',
            'image': create_test_image,
            'author_id' : user.id,
        }
        response = client.post(
            "/api/v1/post/create-post/",
            data=payload,
            **headers
            )
        assert response.status_code == 200
        data = response.json()
        assert data.get('success') == 'OK'
        post_save_model = post_model.objects.all()
        assert len(post_save_model) == 1
        post = post_model.objects.filter(author_id = user.id).first()
        assert post.title == payload["title"]
        assert post.body == payload["body"]
        assert post.author_id == payload["author_id"]



    def test_should_create_like_unlike(self, client, employee_user, auth_headers, post_create, post_like_model):
        """
            Test should create like unlike
        """
        user = employee_user
        headers = auth_headers(user)
        payload = {
            'id' : int(1),
        }
        response = client.post(
            '/api/v1/post/like/',
            data=payload,
            **headers
            )
        assert response.status_code == 200
        data = response.json()
        assert data.get('like') == 'OK'
        post_like = post_like_model.objects.all()
        assert len(post_like) == 1
        assert user.id == 1

        payload = {
            'id' : int(1),
        }
        response = client.post(
            '/api/v1/post/like/',
            data=payload,
            **headers
            )
        assert response.status_code == 200
        data = response.json()
        assert data.get('unlike') == 'OK'
        post_unlike = post_like_model.objects.all()
        assert len(post_unlike) == 0
        assert user.id == 1


    def test_should_analytics_date(self, client, employee_user, auth_headers, post_like_create, post_like_model):
        """
            Test should get analytics
        """
        user = employee_user
        headers = auth_headers(user)
        with freeze_time('2021-05-16'):
            payload = {
                'date_from' : '2021-05-16',
                'date_to' : '2021-05-17',
            }
            response = client.get(
                '/api/v1/post/analitics?date_from=2021-05-16&date_to=2021-05-17',
                data=payload,
                **headers
                )
            assert response.status_code == 301
            post_like_count = post_like_model.objects.all()
            assert len(post_like_count) == 1
