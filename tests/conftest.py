"""
    Conftest
"""

import os
import pytest
from rest_framework_simplejwt.tokens import RefreshToken
from functools import partial
from user.models import User
from post.models import Post, PostLike
from rest_framework.test import APIRequestFactory
from io import BytesIO
from PIL import Image


@pytest.fixture(scope='session')
def django_db():
    settings.DATABASES['default'] = {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'db.sqlite3',
    }

@pytest.fixture
def api_client():
    return APIRequestFactory()

@pytest.fixture
def auth_headers():
    """
        Fixture should create auth headers
    """
    return partial(_auth_headers)


def _auth_headers(user):
    token = RefreshToken.for_user(user)
    return {
        "HTTP_AUTHORIZATION": f"JWT {token.access_token}"
    }


@pytest.fixture
def employee_user():
    user = User.objects.create_user("some_user@mail.com", "user", "password", "password",)
    user.save()
    return user

@pytest.fixture
def post_create(employee_user):
    post = Post.objects.create(
        title = "some_title",
        author = employee_user,
        body = "somebody",
        image = "some_image",
    )
    post.save()
    return post

@pytest.fixture
def post_like_create(employee_user, post_create):
    post_like = PostLike.objects.create(
        user = employee_user,
        post = post_create,
        )
    post_like.save()
    return post_like



@pytest.fixture
def create_test_image():
    file = BytesIO()
    image = Image.new('RGBA', size=(50, 50), color=(155, 0, 0))
    image.save(file, 'png')
    file.name = 'test.png'
    file.seek(0)
    return file

@pytest.fixture
def post_model():
    return Post

@pytest.fixture
def post_like_model():
    return PostLike