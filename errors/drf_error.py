from rest_framework.exceptions import APIException

class BaseError(APIException):
    status_code = 400


raise_error = BaseError