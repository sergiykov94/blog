from django.contrib import admin
from .models import Post, PostLike
# Register your models here.


class PostLikeAdmin(admin.TabularInline):
    model = PostLike

class PostAdminModel(admin.ModelAdmin):
    list_display = ['user', 'post', 'created']

class PostAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'author',
                    'body', 'created', 'image']
    list_filter = ['title', 'author']
    inlines = [PostLikeAdmin]

admin.site.register(Post, PostAdmin)
admin.site.register(PostLike, PostAdminModel)