from rest_framework import serializers

class PostActionSerializer(serializers.Serializer):

    id = serializers.IntegerField()

class AnaliticsSerializer(serializers.Serializer):
    date_from = serializers.DateField()
    date_to = serializers.DateField()

class CreatePostSerializer(serializers.Serializer):

    title = serializers.CharField(max_length = 255)
    body = serializers.CharField(max_length = 255)
    image = serializers.ImageField()
