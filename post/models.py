from django.db import models
from user.models import User
from django.utils.html import mark_safe
import os
# Create your models here.

class Post(models.Model):
    title = models.CharField(max_length = 255)
    author = models.ForeignKey(
        User, on_delete=models.CASCADE,
        null=True, blank=True)
    likes = models.ManyToManyField("PostLike", related_name='post_user', blank=True)
    body = models.TextField(max_length = 200)
    created = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(blank=True)

    def __str__(self):
        return self.title



class PostLike(models.Model):
    user = models.ForeignKey(
        User, on_delete=models.CASCADE
        )
    post = models.ForeignKey(
        "Post", on_delete=models.CASCADE
        )
    created = models.DateTimeField(auto_now_add=True)
