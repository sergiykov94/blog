from django.urls import path
from .views import PostLikeView, CreatePost, AnaliticView

app_name = 'post'

urlpatterns = [
    path('like/', PostLikeView.as_view(), name='postlike'),
    path('create-post/', CreatePost.as_view(), name='create_form'),
    path('analitics/', AnaliticView.as_view(), name='analitics'),
]