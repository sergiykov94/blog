from user.models import User
from post.models import Post, PostLike
from .serializers import (
    PostActionSerializer, 
    CreatePostSerializer, 
    AnaliticsSerializer,
)
from rest_framework.views import APIView
from rest_framework import filters, generics, permissions
from rest_framework.permissions import IsAuthenticated, BasePermission, SAFE_METHODS
from post.adapters import (
    raise_error, response_,
)
from datetime import datetime, timedelta
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.response import Response


class PostLikeView(APIView):
    """
        Api should create : like or dislike
    """
    permission_classes = [IsAuthenticated]

    def post(self, request):
        serializer = PostActionSerializer(data=request.data)
        # import pdb; pdb.set_trace()
        if not serializer.is_valid():
            raise_error(400, serializer.errors)

        data = serializer.validated_data
        post_id = data.get('id')

        post_query_set = Post.objects.filter(id=post_id)
        if not post_query_set.exists():
            raise_error(400, serializer.errors)

        post_instance = post_query_set.first()

        user_like_exist = PostLike.objects.filter(
            post_id=post_instance.id,
            user_id=request.user.id
        ).first()
        if not user_like_exist:
            created_like = PostLike.objects.create(
                user_id=request.user.id,
                post_id=post_instance.id
            )
            post_instance.likes.add(created_like.id)
            return response_({'like' : 'OK'}, 200)

        post_instance.likes.remove(post_instance.id)
        user_like_exist.delete()
        return response_({'unlike' : 'OK'}, 200)


class CreatePost(APIView):
    """
        Api should create post 
    """
    permission_classes = [IsAuthenticated]

    def post(self, request):
        serializer = CreatePostSerializer(data=request.data)

        if not serializer.is_valid():
            raise_error(400, serializer.errors)

        data = serializer.validated_data
        title = data.get('title')
        body = data.get('body')
        image = data.get('image')
        post_create = Post.objects.create(
            title = title,
            body = body,
            author = request.user,
            image = image,
        )
        return response_({'success' : 'OK'}, 200)



class AnaliticView(APIView):
    """
        Api analytics date_from, date_to
    """
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        serializer = AnaliticsSerializer(data=request.GET)

        if not serializer.is_valid():
            raise_error(400, serializer.errors)

        data = serializer.validated_data
        total_likes = PostLike.objects.filter(
            created__gte=data['date_from'],
            created__lte=data['date_to']
        ).count()
        response_data = {
            'total_likes': total_likes
        }
        return Response(response_data) 
