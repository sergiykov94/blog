FROM python:3.8

RUN mkdir /blog
WORKDIR /blog

COPY . /blog/

RUN pip3 install -r requirements.txt
