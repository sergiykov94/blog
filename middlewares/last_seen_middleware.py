
from blog.settings import DISALLOW_LAST_SEEN_URLS

def last_seen_middleware(get_response):

    def middleware(request):
        response = get_response(request)
        allow_set_last_seen = request.path not in DISALLOW_LAST_SEEN_URLS
        if request.user.is_authenticated and allow_set_last_seen:
            request.user.update_last_seen()

        return response
    return middleware